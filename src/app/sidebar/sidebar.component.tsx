import React from 'react';
import './sidebar.component.css';
import { AppstoreOutlined, MailOutlined, SettingOutlined } from '@ant-design/icons';
import type { MenuProps } from 'antd';
import { Menu } from 'antd';

type MenuItem = Required<MenuProps>['items'][number];

function getItem(
  label: React.ReactNode,
  key?: React.Key | null,
  icon?: React.ReactNode,
  children?: MenuItem[],
  type?: 'group',
): MenuItem {
  return {
    key,
    icon,
    children,
    label,
    type,
  } as MenuItem;
}

const items: MenuItem[] = [
  getItem('Bài viết', 'sub1', <MailOutlined />, [
    getItem('Tạo bài viết', '1'),
  ]),

  getItem('Quản lý bài viết', 'sub2', <AppstoreOutlined />, [
    getItem('Danh sách bài đăng', '2'),
    getItem('Danh sách bài duyệt', '3'),
  ]),
];

const onClick: MenuProps['onClick'] = (e) => {
  console.log('click', e);
};

const Sidebar: React.FC = () => (
  <Menu onClick={onClick} style={{ width: 256 }} mode="vertical" items={items} />
);

export default Sidebar;